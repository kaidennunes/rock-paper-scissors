#include "stdafx.h"
#include "Player.h"
#include <iomanip>

using namespace std;


Player::Player(string startingName)
{
	name = startingName;
	numbOfDraws = 0;
	numbOfLosses = 0;
	numbOfWins = 0;
}

string Player::getRPSThrow() const
{
	while (true)
	{
		int randomNumber = rand() % 10;
		if (randomNumber == 0 || randomNumber == 3 || randomNumber == 6)
		{
			return "Paper";
		}
		else if (randomNumber == 1 || randomNumber == 4 || randomNumber == 7)
		{
			return "Rock";
		}
		else if (randomNumber == 2 || randomNumber == 5 || randomNumber == 8)
		{
			return "Scissors";
		}
	}
}

double Player::getWinRecord() const
{
	double totalGames = numbOfDraws + numbOfWins + numbOfLosses;
	if (totalGames <= 0)
	{
		return 0;
	}
	else
	{
		return numbOfWins / totalGames;
	}
}

string Player::toString() const
{
	stringstream ss;
	ss << "Name: " << name << endl;
	ss << "Number of Wins: " << numbOfWins << endl;
	ss << "Number of Losses: " << numbOfLosses << endl;
	ss << "Number of Draws: " << numbOfDraws << endl;
	ss << "Win Record: " << fixed << setprecision(2)<< (getWinRecord() * 100) << "%"<< endl;
	return ss.str();
}

Player::~Player()
{
}
