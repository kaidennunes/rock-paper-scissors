#pragma once
#include <iostream>
#include <string>
#include <sstream>

using namespace std;
class Player
{
public:
	string name;
	int numbOfWins;
	int numbOfLosses;
	int numbOfDraws;

	Player(string startingName);
	string getRPSThrow() const;
	double getWinRecord() const;
	string toString() const;
	~Player();
};

