// RockPaperScissors.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Player.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <time.h>

using namespace std;
/*
	TEST CASE #1

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
1

There are no players yet

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
2

What is the new player's name?
Jason

Jason has been added to the players

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
1

Name: Jason
Number of Wins: 0
Number of Losses: 0
Number of Draws: 0
Win Record: 0.00%

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
5

There aren't enough players in the lineup to fight

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
6

Goodbye


	TEST CASE #2

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
f

Please choose a valid menu option using its labeled number
2

What is the new player's name?
Something

Something has been added to the players

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
3

What player do you want to add to the lineup?
Something

Something has been added to the lineup

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
5

There aren't enough players in the lineup to fight

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
6

Goodbye



	TEST CASE #3

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
2

What is the new player's name?
a

a has been added to the players

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
2

What is the new player's name?
s

s has been added to the players

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
3

What player do you want to add to the lineup?
a

a has been added to the lineup

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
3

What player do you want to add to the lineup?
s

s has been added to the lineup

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
5
a threw a Rock

s threw a Scissors

a wins!

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
1

Name: a
Number of Wins: 1
Number of Losses: 0
Number of Draws: 0
Win Record: 100.00%

Name: s
Number of Wins: 0
Number of Losses: 1
Number of Draws: 0
Win Record: 0.00%

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
4

There are no players in the lineup yet

1. Show Players
2. Add Player
3. Add to Line-up
4. Show Line-up
5. Fight
6. Quit
6

Goodbye
*/

int findIndexOfPlayer(vector<Player*> players, string name){
	int index = -1;
	for (int i = 0; i < players.size(); i++)
	{
		if (players[i]->name.compare(name) == 0)
		{
			index = i;
		}
	}
	return index;
}
void showPlayers(vector<Player*> allPlayers)
{
	cout << "\n";
	// Print out the players if there are any in the vector
	if (allPlayers.size() > 0)
	{
		for (int i = 0; i < allPlayers.size(); i++)
		{
			cout << allPlayers[i]->toString() << endl;
		}
	}
	else
	{
		cout << "There are no players yet" << endl << endl;
	}
}

void addPlayer(vector<Player*> &allPlayers)
{
	cout << "\nWhat is the new player's name?" << endl;
	string name;
	cin.ignore(1000, '\n');
	std::getline(cin, name);

	// Find if name already exists
	int index = findIndexOfPlayer(allPlayers, name);
	if (index >= 0)
	{
		cout << "\nThat player already exists." << endl << endl;
	}
	else
	{
		cout << "\n" << name << " has been added to the players" << endl << endl;
		allPlayers.push_back(new Player(name));
	}
}

void addToLineUp(vector<Player*> &lineUpPlayers, vector<Player*> allPlayers)
{
	if (allPlayers.size() > 0)
	{
		cout << "\nWhat player do you want to add to the lineup?" << endl;
		string name;
		cin.ignore(1000, '\n');
		std::getline(cin, name);

		int index = findIndexOfPlayer(allPlayers, name);
		// Find if name already exists

		if (index >= 0)
		{
			cout << "\n" << name << " has been added to the lineup" << endl << endl;
			lineUpPlayers.push_back(allPlayers[index]);
		}
		else
		{
			cout << "\n" << name << " isn't one of the players" << endl << endl;
		}
	}
	else
	{
		cout << "\nThere are no players to add to the lineup" << endl << endl;
	}
}

void showLineUp(vector<Player*> lineUpPlayers)
{
	cout << "\n";
	// Print out the players if there are any in the vector
	if (lineUpPlayers.size() > 0)
	{
		for (int i = 0; i < lineUpPlayers.size(); i++)
		{
			cout << lineUpPlayers[i]->toString() << endl;
		}
	}
	else
	{
		cout << "There are no players in the lineup yet" << endl << endl;
	}
}

void fight(vector<Player*> &lineUpPlayers, vector<Player*> &allPlayers)
{
	if (lineUpPlayers.size() >= 2)
	{
		const int firstIndex = 0;
		const int secondIndex = 1;

		if (lineUpPlayers[firstIndex]->name.compare(lineUpPlayers[secondIndex]->name) == 0)
		{
			cout << "\n" << lineUpPlayers[firstIndex]->name << " fought against himself and had a draw!" << endl << endl;
			lineUpPlayers[firstIndex]->numbOfDraws += 1;
		}
		else
		{
			string firstPlayerThrow = lineUpPlayers[0]->getRPSThrow();
			string secondPlayerThrow = lineUpPlayers[1]->getRPSThrow();


			cout << lineUpPlayers[firstIndex]->name << " threw a " << firstPlayerThrow << endl << endl;
			cout << lineUpPlayers[secondIndex]->name << " threw a " << secondPlayerThrow << endl << endl;
			if (firstPlayerThrow.compare(secondPlayerThrow) == 0)
			{
				cout << "It was a draw!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfDraws += 1;
				lineUpPlayers[secondIndex]->numbOfDraws += 1;
			}
			else if (firstPlayerThrow.compare("Paper") == 0 && secondPlayerThrow.compare("Rock") == 0)
			{
				cout << lineUpPlayers[firstIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfWins += 1;
				lineUpPlayers[secondIndex]->numbOfLosses += 1;
			}
			else if (firstPlayerThrow.compare("Paper") == 0 && secondPlayerThrow.compare("Scissors") == 0)
			{
				cout << lineUpPlayers[secondIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfLosses += 1;
				lineUpPlayers[secondIndex]->numbOfWins += 1;
			}
			else if (firstPlayerThrow.compare("Rock") == 0 && secondPlayerThrow.compare("Scissors") == 0)
			{
				cout << lineUpPlayers[firstIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfWins += 1;
				lineUpPlayers[secondIndex]->numbOfLosses += 1;
			}
			else if (firstPlayerThrow.compare("Rock") == 0 && secondPlayerThrow.compare("Paper") == 0)
			{
				cout << lineUpPlayers[secondIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfLosses += 1;
				lineUpPlayers[secondIndex]->numbOfWins += 1;
			}
			else if (firstPlayerThrow.compare("Scissors") == 0 && secondPlayerThrow.compare("Paper") == 0)
			{
				cout << lineUpPlayers[firstIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfWins += 1;
				lineUpPlayers[secondIndex]->numbOfLosses += 1;
			}
			else if (firstPlayerThrow.compare("Scissors") == 0 && secondPlayerThrow.compare("Rock") == 0)
			{
				cout << lineUpPlayers[secondIndex]->name << " wins!" << endl << endl;
				lineUpPlayers[firstIndex]->numbOfLosses += 1;
				lineUpPlayers[secondIndex]->numbOfWins += 1;
			}
		}
		lineUpPlayers.erase(lineUpPlayers.begin(), lineUpPlayers.begin() + 2);
	}
	else
	{
		cout << "\nThere aren't enough players in the lineup to fight" << endl << endl;
	}
}

int main()
{
	srand(time(0));
	vector<Player*> allPlayers;
	vector<Player*> lineUpPlayers;
	while (true)
	{
		// This is the menu
		cout << "1. Show Players" << endl;
		cout << "2. Add Player" << endl;
		cout << "3. Add to Line-up" << endl;
		cout << "4. Show Line-up" << endl;
		cout << "5. Fight" << endl;
		cout << "6. Quit" << endl;
		int menuInput;
		while (!(cin >> menuInput))
		{
			cin.clear();
			cin.ignore(1000, '\n');
			cout << "\nPlease choose a valid menu option using its labeled number" << endl;
		}
		if (menuInput == 1)
		{
			showPlayers(allPlayers);
		}
		else if (menuInput == 2)
		{
			addPlayer(allPlayers);
		}
		else if (menuInput == 3)
		{
			addToLineUp(lineUpPlayers, allPlayers);
		}
		else if (menuInput == 4)
		{
			showLineUp(lineUpPlayers);
		}
		else if (menuInput == 5)
		{
			fight(lineUpPlayers, allPlayers);
		}
		else if (menuInput == 6)
		{
			cout << "\nGoodbye" << endl;
			break;
		}
		else
		{
			cout << "\nThis is not one of the menu options" << endl;
		}
	}
	system("pause");
	return 0;
}

